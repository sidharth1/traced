#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <udis86.h>
#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/user.h>
#include <sys/reg.h>

pid_t pid = 0;
long int p = 0; //process_id
struct user_regs_struct regs;
unsigned long int stream = 0; //output stream of bytes
unsigned char buff[16];

ud_t ud_obj;

static void attach_now(char *tpid);
static void single_step();

/* Steps:
 * 1> Attach to specified pid
 * 2> Singlestep and print the instructions
 */
int main(int argc, char *argv[]){
	if(argc != 3){
		printf("ERROR: wrong number of arguments.\nUsage: ./itrace -p <process_id>\n");
		exit(-1);
	}
	if(argv[2] == NULL){
		printf("ERROR: kindly specify a process id\nUsage: ./itrace -p <process_id>\n");
		exit(-1);
	}
	//attach to process
	attach_now(argv[2]);
	//set disassembler options
	ud_init(&ud_obj);
	ud_set_mode(&ud_obj, 32);
	ud_set_syntax(&ud_obj, UD_SYN_INTEL);
	//single step
	single_step();
	return 0;
}

/* Attach to process with pid - p
 * and put the returned pid in - p_ret
 */
static void attach_now(char *tpid){
	int status = 0;
	p = strtol(tpid, NULL, 10);
	long p_ret = 0;

	//attach to process
	p_ret =	ptrace(PTRACE_ATTACH, p, NULL, NULL);
	//check for a attach
	if(p_ret == -1){
		printf("ERROR: Failed to attach using PTRACE_ATTACH\n");
		exit(-1);
	}

	//wait for tracee to recieve stop signal
	pid = waitpid(p, &status, WUNTRACED | WCONTINUED);
	//check if child was stopped via a SIGSTOP hence
	//successful attach
	if(WIFSTOPPED(status)){
		printf("Successfully attached to the process %ld\n", p);
	}
	else printf("ERROR: SIGSTOP could not be sent to the child process");

	return;
}

/* Singlestep through the child 
 * and get the instruction pointed to by eip.
 * Disassemble the instruction and print it.
 */
static void single_step(){
	int status = 0;
	int i = 0;
	unsigned char *ptr = buff; //an iterator/reference for the buffer

	//singlestep, then wait for signal,
	//populate the registers in the structure 'regs'
	//get instruction from the address in 'regs.eip' into 'stream'
	//  -copy the 'stream' into buffer 'buff' and repeat
	//now disassemble the buffer 'buff'
	while(1){
		ptrace(PTRACE_SINGLESTEP, pid, NULL, NULL);
		waitpid(p, &status, 0);
		ptrace(PTRACE_GETREGS, pid, NULL, &regs);
		stream = ptrace(PTRACE_PEEKTEXT, pid, regs.eip, NULL);
		while(i<4){
			memcpy(ptr, &stream, 4);
			ptr += 4;
			stream = ptrace(PTRACE_PEEKTEXT, pid, regs.eip+(i*4), NULL);
			i++;
		}
		ud_set_input_buffer(&ud_obj, buff, 15);
		if(ud_disassemble(&ud_obj)){
			printf("\t%s\n", ud_insn_asm(&ud_obj));
		}
		i = 0;
		ptr = &buff[0]; //reset pointer
	}
	return;
}


